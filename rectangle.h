
#pragma once

#include <GL/gl.h>

#ifndef PEG_H
#define PEG_H
	#include "peg.h"
#endif 

#include "utility.h"

// Child class of peg
class rectangle : public peg
{
	public:
	// Constructor
	rectangle( glm::vec3 _pegCentre ); //: public peg( glm::vec3 _pegCentre );

	// Outer Coordinates of the shape
	glm::vec3 topLeft, topRight, bottomLeft, bottomRight;

	// Dimensions of the shape
	const float width   = 0.6;
	const float height  = 0.3;

	const GLfloat rectColor[4] = { 0.0, 0.75, 0.0, 1.0 };

	// Draw shape OVERRIDDEN from peg class
	virtual void draw( void );

	// Override the virtual abstract method inherited
	virtual bool checkImpact( glm::vec3 position, float radius, bool isFuture );

};
