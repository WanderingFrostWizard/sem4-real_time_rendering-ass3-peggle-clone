
#include <glm/glm.hpp>	// for vec3

// ABSTRACT CLASS FOR PEGS

class peg
{
	public:
	// peg( glm::vec3 _pegCentre ) {};
	peg() {};

	// Destructor
	virtual ~peg( ) {};

	
	// Centre point for the peg INHERITED by children
	glm::vec3 pegCentre;

	// Used to store if the peg has been hit
	bool collision;

	const GLfloat hitColor[4] = { 0.8, 0.0, 0.0, 1.0 };

	// Virtual function to create abstract class
	virtual bool checkImpact( glm::vec3 position, float radius, bool isFuture ) = 0;

	// Draw shape INHERITED
	virtual void draw( void ) = 0;

	void resetCollision( void )
	{
		collision = false;
	}
};