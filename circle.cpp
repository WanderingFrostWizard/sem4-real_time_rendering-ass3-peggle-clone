
#include "circle.h"

circle::circle( glm::vec3 _pegCentre ) : peg () // : public peg::peg()
{
	pegCentre = _pegCentre;
	collision = false;
}

bool circle::checkImpact( glm::vec3 position, float _radius, bool isFuture )
{
	// BALL vs CIRCLE
	float sum_radii, sum_radii_sq, xDist, yDist, dist_sq;

	sum_radii = _radius + radius;
	sum_radii_sq = sum_radii * sum_radii;
	xDist = pegCentre.x - position.x;
	yDist = pegCentre.y - position.y;
	dist_sq = xDist * xDist + yDist * yDist;
	if (dist_sq <= sum_radii_sq) 
	{	
		// Only mark collision if this is a NON future calculation
		if ( !isFuture )
			collision = true;

		return true;
	}
	else 
		return false;
}

void circle::draw( )
{
	glPushMatrix();
	glPushAttrib(GL_CURRENT_BIT);
		glTranslatef( pegCentre.x, pegCentre.y, 0 );
		if ( collision )
		{
			glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, hitColor );
			glColor4fv( hitColor );
		}
		else 
		{
			glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, circleColor );
			glColor4fv( circleColor );
		}
		glutSolidSphere( radius, 8, 8 );
	glPopAttrib();
	glPopMatrix();
}
