
#include "rectangle.h"
#include "circle.h"
#include "utility.h"

class board
{
	// Dimensions of the board
	protected:
		bool debugLayouts = false;

		const float height = 9.8;
		const float width = 9.8;

		const GLfloat edgeColor[4] = { 0.75, 0.75, 0.75, 1.0 };

		// Number of peg rows
		const int ROWS = 10;

		// Variables to calculate peg positions
		int pegsPerRow;
		float totalwidth;
		float stepx, stepy;
		int nextPegID;

	public:
		enum boardType { GRID, OFFGRID, TESTGRID };

		boardType currentBoardType;

		// ARRAY OF ELEMENTS ON BOARD
		peg ** pegArray;

		// Boundaries of the board
		glm::vec3 topLeft, topRight, bottomLeft, bottomRight;

		int numPegs;

		void setBoardType( void );

		// Constructor
		board( enum boardType, int numPegs );

		~board( void );

		void setupPegGrid( int numPegs, bool isOffset );
		void setupTestGrid( void );
		void initRemainingPegs( int startID );

		void drawBoard( void );
		void drawEdge( void );
		float getWidth( void );

		// Returns a string with the current layout name. Used for the OSD
		const char* getLayoutName( void );
};