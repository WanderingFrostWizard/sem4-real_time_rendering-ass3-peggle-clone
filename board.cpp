#include "board.h"

#include <iostream>

/*
 * Select the boardtype chosen by the user (using class variable 
 * currentBoardType) and the run the corresponding function to draw the grid
 */
void board::setBoardType( void )
{
	switch( currentBoardType )
	{
		case boardType::GRID:
			if ( debugLayouts )
				std::cout << "GRID Layout" << std::endl;
			setupPegGrid( numPegs, false );
		break;

		case boardType::OFFGRID:
			if ( debugLayouts )
				std::cout << "OFFSET GRID Layout" << std::endl;
			setupPegGrid( numPegs, true );
		break;		

		case boardType::TESTGRID:
			if ( debugLayouts )
				std::cout << "TESTING GRID" << std::endl;
			setupTestGrid();
		break;

		default:
		break;
	}
}

// Constructor
board::board( enum boardType type, int _numPegs ) : currentBoardType( type ), numPegs( _numPegs )
{
	// Define the boundaries of the gameboard
	calculateVerts( glm::vec3 (0, 0, 0 ), 
			&topLeft, 
			&topRight, 
			&bottomLeft, 
			&bottomRight,
			height * 2 ,
			width * 2  );

	// Define array of pegs
	pegArray = new peg*[ numPegs ];

	/*
	 * Note the following function does use the type variable, but it gets it 
	 * from the class variable currentBoardType that was assigned above 
	 */
	setBoardType( );
} 

// Destructor.  
board::~board( )
{
	for (int i = 0; i < numPegs; i++ )
	{
		delete pegArray[i];
	}
	delete[] pegArray;
}


/*
 * Add Pegs into a square grid arrangement by dividing the number of pegs into
 * rows, and evenly distributing them, based on the variables ROWS and numPegs
 *
 *        offset = false                offset = true
 *     -------------------           -------------------
 *     |        x        |           |        x        |
 *     |                 |           |                 |
 *     |                 |           |                 |
 *     |  #   #   #   #  |           |  #   #   #   #  |
 *     |  #   #   #   #  |           |    #   #   #    |
 *     |  #   #   #   #  |           |  #   #   #   #  |
 *     |  #   #   #   #  |           |    #   #   #    |
 *     |                 |           |                 |
 *     |                 |           |                 |
 *     |      |___|      |           |      |___|      |
 *     -------------------           -------------------
 */
void board::setupPegGrid( int numPegs, bool isOffset )
{
	float x,y;
	int i, j;

	// Initialize class variable to 0
	nextPegID = 0;

	if ( numPegs < (ROWS * 5) )
		std::cout << "ERROR need to have at LEAST " << ROWS << " x 5 pegs to fill board CURRENTLY HAVE " << numPegs << std::endl;

	// Calculate the number of pegs to display in each row
	pegsPerRow = numPegs / ROWS;

	// Total width of the board displayed (-width -> 0 -> width) hence 2x width
	totalwidth = width * 2;

	stepx = totalwidth / pegsPerRow;

	/* 
	 * The board is 2 x height high, but I only want to use the centre half of
	 * the board
	 */
	stepy = height / ROWS;

	for ( j = 0; j < ROWS; j++ )
	{
		// Normal Row (GRID WILL ONLY use this loop)
		if ( (isOffset) && ( j % 2 == 0 ) )
		{
			for ( i = 0; i < pegsPerRow; i++ )
			{
				// nextPegID = ( j * pegsPerRow ) + i;

				//       Start position    +     Step
				x = -width + (0.5 * stepx) + (i * stepx);

				//  Start position    +   Step	
				y = (0.5 * height) - (j * stepy);

				pegArray[ nextPegID ] = new circle( glm::vec3 ( x, y, 0 ) );

				nextPegID++;
			}
		}
		else
		{
			// Offset Row
			// Need one less peg per row in this row
			for ( i = 0; i < pegsPerRow - 1; i++ )
			{
				/* NOTE THIS WILL CREATE NULL ENTRIES IN THE ARRAY, AS EVERY 
				 * SECOND LINE WILL ONLY HAVE pegsPerRow -1 pegs BUT THIS
				 * ARRAY IS ALREADY CHECKED ELSEWHERE FOR NULL ENTRIES
				 * ### UPDATE ###  USING A COUNTER VARIABLE INSTEAD
				 */
				// nextPegID = ( j * pegsPerRow ) + i;

				//       Start position    +     Step
				x = -width + ( stepx ) + (i * stepx);

				//  Start position    +   Step	
				y = (0.5 * height) - (j * stepy);

				pegArray[ nextPegID ] = new circle( glm::vec3 ( x, y, 0 ) );

				nextPegID++;
			}
		}
	}	

	// Initialize remaining pegs to NULL
	if ( nextPegID < numPegs )
		initRemainingPegs( nextPegID );
}

/*
 * Add Pegs to the board to simply test collisions and other functions
 *
 *     -------------------
 *     |        x        |
 *     |                 |
 *     |                 |
 *     |     R  O        |
 *     |     R  O        |
 *     |                 |
 *     |                 |
 *     |                 |
 *     |                 |
 *     |      |___|      |
 *     -------------------
 */
void board::setupTestGrid( void )
{
	// TESTING PEG POSITIONS AND COLLISIONS
	pegArray [0] = new rectangle( glm::vec3 ( -1, 1, 0 ) );
	pegArray [1] = new rectangle( glm::vec3 ( -1, 2, 0 ) );

	pegArray [2] = new circle( glm::vec3 ( 0.15, 2, 0 ) );
	pegArray [3] = new circle( glm::vec3 ( 2, 2, 0 ) );

	initRemainingPegs( 4 );
}

void board::initRemainingPegs( int startID )
{
	for ( int i = startID; i < numPegs; i++ )
	{
		pegArray [i] = NULL;
	}
}

void board::drawBoard( void ) 
{
	for ( int i = 0; i < numPegs; i++ )
	{
		if ( (pegArray[i] != NULL) && ( !pegArray[i]->collision ) )
		{
			pegArray[i]->draw();
		}
	}
}

void board::drawEdge( void )
{
	glPushAttrib(GL_CURRENT_BIT);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, edgeColor );
	glBegin(GL_LINE_STRIP);
		glColor4fv ( edgeColor );
		glVertex3fv(&topLeft[0]);
		glVertex3fv(&topRight[0]);
		glVertex3fv(&bottomRight[0]);
		glVertex3fv(&bottomLeft[0]);
		glVertex3fv(&topLeft[0]);
	glEnd();
	glPopAttrib();
}

float board::getWidth( void )
{
	return width;
}

const char* board::getLayoutName( void )
{
	switch( currentBoardType )
	{
		case boardType::GRID:
			return "GRID";
		break;

		case boardType::OFFGRID:
			return "OFFSET GRID";
		break;

		case boardType::TESTGRID:
			return "TESTING GRID";
		break;

		default:
			return "ERROR";
		break;
	}
}