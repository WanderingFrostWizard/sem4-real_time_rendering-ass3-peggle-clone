CC=g++

LFLAGS= -lglut -lGLU -lGL -lm -g
CFLAGS= -Wall -pedantic -std=c++14

HEADERS =  
#OBJECTS := main.o board.o ball.o game.o rectangle.o  
OBJECTS := main.o board.o    
BIN := ass

#all: $(BIN)
#all: tute
all: FULL

$(BIN): $(OBJECTS)
	$(CC) $(OBJECTS) -o $(BIN) $(LFLAGS) 		#$(CFLAGS) 

$(OBJECTS): %.o: %.cpp
	$(CC) $(CFLAGS) -c $< -o $@



FULL:
	$(CC) $(CFLAGS) *.cpp -o $(BIN) $(LFLAGS)


#FULL:
#	g++ -std=c++14 shaders.c sinewave3D-glm.cpp -lGLEW -lglut -lGLU -lGL -lm

# Remove objects and binaries
clean:
		rm   $(BIN)
