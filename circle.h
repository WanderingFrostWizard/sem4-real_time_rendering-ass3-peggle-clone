
#pragma once

#include <GL/gl.h>
#include <GL/glut.h>
#include <glm/glm.hpp>	// for vec3

#ifndef PEG_H
#define PEG_H
	#include "peg.h"
#endif 

// Child class of peg
class circle : public peg
{
	public:
	// Constructor
	circle( glm::vec3 _pegCentre ); //: public peg( glm::vec3 _pegCentre );

	// Destructor
	virtual ~circle() {}

	// Dimensions of the shape
	const float radius = 0.2;

	const GLfloat circleColor[4] = { 0.0, 0.5, 0.0, 1.0 };

	// Draw shape OVERRIDDEN from peg class
	virtual void draw( void );

	// Override the virtual abstract method inherited
	virtual bool checkImpact( glm::vec3 position, float radius, bool isFuture );
};
