#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <glm/glm.hpp>  // For vec3
#include <math.h>
#include <sys/time.h>
#include <iostream>
#include <locale>		// for toLower()


#include "board.h"

// Rendering info
enum renderMode { wire, solid };
static renderMode renMode = solid;

// Debugging flags
struct Debug
{
	bool collisions;
	bool showOSD;
	bool futureMoves;
	float renderSpeed;
}debug = { false, false, false, 1 };

// Game Variables
int numPegs = 100;
const int MINPEGS = 50;
const int MAXPEGS = 200;

const int STARTINGNOOFBALLS = 3;

// Points for hitting a peg
const int PEGVALUE = 10;
// Bonus points for landing in the catcher
const int CATCHERBONUS = 100;

struct gameVars
{
	int ballsRemaining;
	int score;
	bool gameOver;
	bool extraBall;
} g = { STARTINGNOOFBALLS, 0, false, false };

board * gameBoard;

const GLfloat vectorColor[4] = { 0.5, 0.0, 0.5, 1.0 };
const GLfloat futureColor[4] = { 1.0, 0.0, 0.0, 1.0 };

// FUNCTION PROTOTYPES IN ORDER
void resetBall( void );
void resetGame( void );
void integrate( glm::vec3 * tempPos, glm::vec3 * tempVel );
void pegCollision( peg &p1, glm::vec3 * tempVel );
void collide( glm::vec3 * tempPos, glm::vec3 * tempVel, bool isFuture );
void resetCollisions( void );
void drawBall( void );
void drawVector( void );
void setVelocityDir( glm::vec3 * tempVel );
void calculateTrajectory( void );
void drawTrajectory( void );
void calcCatcherShape( void );
void moveCatcher( float dt );
void drawCatcher( void );
void setRenderMode(renderMode rm);
void changeRenderMode( void );
void keyboardCB(unsigned char key, int x, int y);
void myReshape(int w, int h);
void displayOSD( void );
void display( void );
void update( void );
void changeBoard( void );
void redrawBoard( void );
void myInit(void );
int main( int argc, char** argv);
void cleanup( void );


// ----------- BALL CLASS VARIABLES --------------------------------
	glm::vec3 position;
	glm::vec3 velocity;

	//TODO ######### DECIEDE ON A GRAVITY THAT SUITS THE EQUATIONS BEST ##############################
	const float gravity = -9.8;
	// const float gravity = -4.9;
	// const float gravity = -2.5;
	// const float gravity = -0.98;

	// Angle of the launch vector ranges from -180 (LHS screen) -> 180 (RHS screen)
	float startAngle = 1.5 * M_PI;

	//TODO####################################################
	float speed = 10;
	const float MAXSPEED = 50;

	float radius;   // UNABLE TO MAKE THIS A CONSTANT
	const float mass = 1;

	bool gameStart;

	// Time variables
	const int milli = 1000;
	float t;
	float lastTime;
	float dt;

	const GLfloat ballColor[4] = { 0.0, 0.0, 0.5, 1.0 };
	const GLfloat catcherColor[4] = { 1.0, 1.0, 0.0, 1.0 };


	float catcherPos = 0;
	float catcherVelocity = 2.0;
	const float CATCHERHEIGHT = 0.5;
	const float CATCHERWIDTH = 1.75;
	float catcherTopLip = CATCHERWIDTH * 0.5;

	// Catcher drawing points
	glm::vec3 cTopLeft;
	glm::vec3 cTopRight;
	glm::vec3 cBottomLeft; 
	glm::vec3 cBottomRight;

/*
 * Used to reset the ball to its starting position, after it has been caught by
 * the catcher, or has fallen out the bottom of the screen. 
 * Resets the positon and velocity of the ball
 */
void resetBall()
{
	gameStart = false;

	position.x = 0;
	position.y = 9;
	velocity.x = 0;
	velocity.y = gravity; 

	calculateTrajectory();
}

/*
 * Used to reset the game to its initial state, including reseting all 
 * peg collisions.
 */
void resetGame()
{
	g.ballsRemaining = STARTINGNOOFBALLS;
	g.score = 0;
	g.gameOver = false;

	resetCollisions();
}

// ----------- BALL CLASS FUNCTIONS --------------------------------

// Integrate equations of motion
void integrate( glm::vec3 * tempPos, glm::vec3 * tempVel )
{
	// Calculate new position of the ball
	tempPos->x += debug.renderSpeed * dt * tempVel->x;
	tempPos->y += debug.renderSpeed * dt * tempVel->y;

	tempVel->y += gravity * debug.renderSpeed * dt;
}

// Collision reaction
void pegCollision( peg &p1, glm::vec3 * tempVel )
{
	double n[2], n_mag;
	double projnv2;
	double m1, m2, v1i, v2i, v2f;

	double v1f;

	/* Normal vector n between centres. */
	n[0] = position.x - p1.pegCentre.x;
	n[1] = position.y - p1.pegCentre.y;
	
	/* Normalise n. */
	n_mag = sqrt(n[0] * n[0] + n[1] * n[1]);
	n[0] /= n_mag;
	n[1] /= n_mag;

	/* Vector projection/component/resolute of velocity in n direction. */
	// 			    = PEG velocity 					+ ball velocity
	// projnv1 = n[0] * p1.velocity[0] + n[1] * p1.velocity[1];
	// projnv2 = n[0] * p2.velocity[0] + n[1] * p2.velocity[1];
	projnv2 = n[0] * velocity.x + n[1] * velocity.y;

	/* Use 1D equations to calculate final velocities in n direction. */
	v1i = 0;  //projnv1;
	v2i = projnv2;
	m1 = 3;
	m2 = 1 ;
	v2f = 2.0 * m1 / (m1 + m2) * v1i + (m2 - m1) / (m1 + m2) * v2i;

	// v2f = 0;
	
	/* Vector addition to solve for final velocity. */
	// velocity.x = ( velocity.x - v2i * n[0]) + v2f * n[0];
	// velocity.y = ( velocity.y - v2i * n[1]) + v2f * n[1];


	// MODIFICATION TO INCLUDE THE velocity (negated) to stop the "lost force"
	// STILL CAUSES OCCASIONAL ERRORS
	v1f = (m1 - m2) / (m1 + m2) * v1i + 2.0 * m2 / (m1 + m2) * v2i;
	tempVel->x = ( tempVel->x - v2i * n[0]) + (v2f - v1f) * n[0];
	tempVel->y = ( tempVel->y - v2i * n[1]) + (v2f - v1f) * n[1];

  /*
	v1f = (m1 - m2) / (m1 + m2) * v1i + 2.0 * m2 / (m1 + m2) * v2i;
	v2f = 2.0 * m1 / (m1 + m2) * v1i + (m2 - m1) / (m1 + m2) * v2i;
	
	Vector addition to solve for final velocity. 
	p1.velocity[0] = (p1.velocity[0] - v1i * n[0]) + v1f * n[0];
	p1.velocity[1] = (p1.velocity[1] - v1i * n[1]) + v1f * n[1];
	p2.velocity[0] = (p2.velocity[0] - v2i * n[0]) + v2f * n[0];
	p2.velocity[1] = (p2.velocity[1] - v2i * n[1]) + v2f * n[1];
  */
	// std::cout << "v.x = " << velocity.x << "  v.y = " << velocity.y << std::endl;
}

// Collision detection
void collide( glm::vec3 * tempPos, glm::vec3 * tempVel, bool isFuture )
{
	// Collisions against roof
	if ( tempPos->y >= gameBoard->getWidth() )
	{
		if ( debug.collisions )
			std::cout << "Collision with roof at x = " << tempPos->x << std::endl;
		tempVel->y *= -1;
		// Take one step away from the roof
		tempPos->y += dt * tempVel->y;
		return;
	}

	// Collisions against walls?
	if ( ( tempPos->x - radius ) <= -gameBoard->getWidth() || 
				( tempPos->x + radius ) >= gameBoard->getWidth() ) 
	{
		if ( debug.collisions )
			std::cout << "Collision at ->width = " << gameBoard->getWidth() << std::endl;
		tempVel->x *= -1;
		// Take one step away from the wall
		tempPos->x += dt * tempVel->x;
		return;
	}

	// Ball falls into the catcher
	if ( ( tempPos->y <= -gameBoard->getWidth() + CATCHERHEIGHT ) && 
				( tempPos->x >= catcherPos - catcherTopLip ) && 
				( tempPos->x <= catcherPos + catcherTopLip ) )
	{
		// Show the extraBall message
		g.extraBall = true;

		// Give the bonus points
		g.score += CATCHERBONUS;
		if ( debug.collisions )
			std::cout << "Catcher Collision " << std::endl;
		resetBall();
		return;
	}
	// Ball falls out the bottom of the screen
	else if ( tempPos->y <= -gameBoard->getWidth() )
	{
		// Check if player has any remaining lives (balls)
		if ( g.ballsRemaining == 0 )
		{
			// Stop the ball from moving
			gameStart = false;
			// Flag to display game over message and final score
			g.gameOver = true;
			return;
		}
		else 
		{
			g.ballsRemaining -= 1;
			resetBall();
			return;
		}
	}

	/*
	 * This function is only RUN AFTER each other collision check FAILS. This is 
	 * designed to improve performance, by checking simple calculations, before
	 * the following loop, which runs a brute force check on each pegs position,
	 * against the balls position, and if true, it calls the pegCollision
	 */
	for (int i = 0; i < numPegs - 1; i++) 
	{
		if ( ( gameBoard->pegArray[ i ] != NULL ) && ( !gameBoard->pegArray[ i ]->collision ) )
		{
			if ( gameBoard->pegArray[ i ]->checkImpact( *tempPos, radius, isFuture ) )
			{
				pegCollision( *gameBoard->pegArray[ i ], tempVel );

				// Only score points if this is a NON future calculation
				if ( !isFuture )
				{
					// Add the points to the players score
					g.score += PEGVALUE;
				}
				
				if ( debug.collisions )
					std::cout << "collision" << std:: endl;
			}
		}
	}

}

void resetCollisions()
{
	for (int i = 0; i < numPegs - 1; i++) 
	{
		if ( gameBoard->pegArray[ i ] != NULL )
			gameBoard->pegArray[ i ]->resetCollision();
	}
}

void drawBall()
{
	glPushMatrix();
	glPushAttrib(GL_CURRENT_BIT);
		glTranslatef( position.x, position.y, 0 );
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, ballColor );
		glColor4fv ( ballColor );
		glutSolidSphere( radius, 8, 8 );
	glPopAttrib();
	glPopMatrix();
}

// Draw the balls velocity vector
void drawVector()
{
	float xFinal = position.x + 1.5 * cosf( startAngle );
	float yFinal = position.y + 1.5 * sinf( startAngle );

	glPushMatrix();
	glPushAttrib(GL_CURRENT_BIT);
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, vectorColor );
		glColor4fv( vectorColor );
		glBegin(GL_LINES);
		glVertex3f( position.x, position.y, 0 );
		glVertex3f( xFinal, yFinal, 0 );
		glEnd();
	glPopAttrib();
	glPopMatrix();
}

/* Used to set the initial velocity and angle of the ball when the player
 * launches the ball
 */
void setVelocityDir( glm::vec3 * tempVel )
{
	tempVel->x = speed * cosf( startAngle );
	// Must be += to add this to gravity from resetBall
	tempVel->y += speed * sinf( startAngle );
}

const int NUM_FUTURE_MOVES = 100;
glm::vec3 futureMove [ NUM_FUTURE_MOVES ];

void calculateTrajectory()
{
	// Copy the current velocity into a temp variable
	// glm::vec3 tempVel = velocity;
	glm::vec3 tempVel;
	tempVel.x = velocity.x;
	tempVel.y = gravity;
	tempVel.z = 0;

	// Setup our own dt value, to enable larger gaps in the futureMove points
	const float tempDT = 0.016;
	// const float tempDT = 0.03;
	dt = tempDT;

	// Set starting position
	futureMove[0].x = position.x;
	futureMove[0].y = position.y;
	futureMove[0].z = 0;

	// calculate velocity vector
	setVelocityDir( &tempVel );

	// Integrate the inital velocities and positions
	integrate( &futureMove[0], &tempVel );

	for ( int i = 1; i < NUM_FUTURE_MOVES; i++ )
	{
		// Add previous move
		futureMove[i].x = futureMove[i-1].x;
		futureMove[i].y = futureMove[i-1].y;
		futureMove[i].z = 0;

		integrate( &futureMove[i], &tempVel );

		if ( debug.futureMoves )
			std::cout << "dt = " << dt << " x = " << futureMove[i].x << "  y = " << futureMove[i].y << "  z = " << futureMove[i].z << std::endl;

		// STOP DRAWING IF THE futureMove GOES TOO LOW
		if ( futureMove[i].y < -gameBoard->getWidth() + CATCHERHEIGHT + 0.5 )
			break;

		// Collide
		collide( &futureMove[i], &tempVel, true );
	}
}


void drawTrajectory()
{
	// Draw gl_points at each point stored in the array
	glPushMatrix();
	glPushAttrib(GL_CURRENT_BIT);
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, futureColor );
		glColor4fv ( futureColor );

		if (renMode == wire)
		{
			glBegin(GL_POINTS);
		}	
		else
		{
			glBegin(GL_LINE_STRIP);
		}
	
		for ( int i = 0; i < NUM_FUTURE_MOVES; i++ )
		{	
			// STOP DRAWING IF THE futureMoves GOES TOO LOW
			if ( futureMove[i].y < -gameBoard->getWidth() + CATCHERHEIGHT + 0.5 )
				break;
			glVertex3fv( &futureMove[i][0]  );
		}
		glEnd();
	glPopAttrib();
	glPopMatrix();
}


void calcCatcherShape()
{
	cTopLeft.x = -CATCHERWIDTH + catcherTopLip;
	cTopLeft.y = CATCHERHEIGHT;
	cTopLeft.z = 0;

	cBottomLeft.x = -CATCHERWIDTH;
	cBottomLeft.y = 0;
	cBottomLeft.z = 0;

	cBottomRight.x = CATCHERWIDTH;
	cBottomRight.y = 0;
	cBottomRight.z = 0;

	cTopRight.x = CATCHERWIDTH - catcherTopLip;
	cTopRight.y = CATCHERHEIGHT;
	cTopRight.z = 0;
}

void moveCatcher( float dt )
{
	// Move the catcher along the x-axis
	catcherPos += debug.renderSpeed * dt * catcherVelocity;

	// Change direction of the catcher if it collides with either left or right wall
	if ( (-gameBoard->getWidth() >= catcherPos - CATCHERWIDTH) || (catcherPos + CATCHERWIDTH >= gameBoard->getWidth()) )
	{
		catcherVelocity *= -1;
		if ( debug.collisions )
		{
			std::cout << "Catcher hit Wall at " << catcherPos << " +/- " << CATCHERWIDTH;
			std::cout << " Board width = " << gameBoard->getWidth() << std::endl;
		}
	}
}

/*
 * Draws the catcher shape like so
 *     
 *        /|           |\
 *       / |           | \
 *      /..|...........|..\
 */
void drawCatcher( )
{
	glPushMatrix();
	glPushAttrib(GL_CURRENT_BIT);
		// Move to the centre of the catcher
		glTranslatef( catcherPos, -gameBoard->getWidth() + 0.05, 0 );
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, catcherColor );

		// Draw line at the base of the catcher
		glBegin(GL_LINES);
			glColor4fv ( catcherColor );
			glVertex3fv( &cBottomLeft[0] );
			glVertex3fv(&cBottomRight[0]);
		glEnd();

		// Draw the sides of the catcher
		glBegin(GL_TRIANGLES);
			// Left Triangle
			glVertex3fv(&cTopLeft[0]);
			glVertex3fv(&cBottomLeft[0]);
			glVertex3f( cTopLeft.x, 0, 0 );
			// Right Triangle
			glVertex3f( cTopRight.x, 0, 0 );
			glVertex3fv(&cBottomRight[0]);
			glVertex3fv(&cTopRight[0]);
		glEnd();
	glPopAttrib();
	glPopMatrix();
}

// ----------- END OF BALL CLASS --------------------------------

void setRenderMode(renderMode rm)
{
	if (rm == wire) {
		glDisable(GL_LIGHTING);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_NORMALIZE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	} else if (rm == solid) {
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_NORMALIZE);
		glShadeModel(GL_SMOOTH);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
}

void changeRenderMode()
{
	if (renMode == wire)
		renMode = solid;
	else
		renMode = wire;
	setRenderMode(renMode);
}

void keyboardCB(unsigned char key, int x, int y)
{
	switch ( tolower(key) ) {
		case 27:
			std::cout << "exit" << std::endl;
			cleanup();
			exit(0);
		break;

		case 'r':
			resetGame();
			resetBall();
		break;

		case 'w':
			changeRenderMode();
		break;

		case ' ':
			if ( (!gameStart) && (!g.gameOver) )
			{
				g.extraBall = false;
				setVelocityDir( &velocity );
				gameStart = true;
			}
		break;

		case 'b':
			if ( !gameStart )
			{
				changeBoard();
				std::cout << "Changing Board type" << std::endl;
			}
		break;

		case '<':
		case ',':
				debug.renderSpeed -= 0.1;
				std::cout << "renderSpeed = " << debug.renderSpeed << std::endl;
		break;
		case '>':
		case '.':
				debug.renderSpeed += 0.1;
				std::cout << "renderSpeed = " << debug.renderSpeed << std::endl;
		break;

		case '[':
			if ( ( !gameStart) && ( numPegs > MINPEGS ) )
			{
				numPegs -= 10;
				std::cout << "numPegs = " << numPegs << std::endl;
				redrawBoard();
			}
		break;
		case ']':
			if ( ( !gameStart) && ( numPegs < MAXPEGS ) )
			{
				numPegs += 10;
				std::cout << "numPegs = " << numPegs << std::endl;
				redrawBoard();
			}
		break;

		case '+':
			if ( speed < MAXSPEED )
			{
				speed += 5.0 * dt;
				std::cout << "speed = " << speed << std::endl;
				calculateTrajectory();
			}
			break;
		case '-':
			if ( speed > 0.1 )
			{
				speed -= 5.0 * dt;
				std::cout << "speed = " << speed << std::endl;
				calculateTrajectory();
			}
			break;
		case 'a':
			if ( startAngle >= M_PI )
			{	
				startAngle -= M_PI/180;
				calculateTrajectory();
			}
		break;
		case 'd':
			if ( startAngle <= 2*M_PI )
			{
				startAngle += M_PI/180;
				calculateTrajectory();
			}
		break;

		case 'o':
			debug.showOSD = !debug.showOSD;
			std::cout << "Toggling OSD" << std::endl;
		break;
	}
	glutPostRedisplay();
}

void myReshape(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(-10.0, 10.0, -10.0, 10.0, -10.0, 10.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void displayOSD()
{
  char buffer[40];
  char *bufp;
  int w, h;
    
  glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT);
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_LIGHTING);

  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();

  /* Set up orthographic coordinate system to match the window, 
   * i.e.
   *                       (w,h)
   *      ----------------x
   *      |               |
   *      |               |
   *      |               |
   *      |               |
   *      |               |
   *      |               |
   *      |               |
   *      x----------------
   * (0,0)
   *
   */
  w = glutGet(GLUT_WINDOW_WIDTH);
  h = glutGet(GLUT_WINDOW_HEIGHT);
  glOrtho(0.0, w, 0.0, h, -1.0, 1.0);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  glColor3f(1.0, 1.0, 0.0);
  glRasterPos2i( 40, h - 30 );
  snprintf(buffer, sizeof buffer, "Peg Layout: %s", gameBoard->getLayoutName() );
  for (bufp = buffer; *bufp; bufp++)
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

  glColor3f(1.0, 1.0, 0.0);
  glRasterPos2i( 40, h - 50 );
  snprintf(buffer, sizeof buffer, "Balls Remaining: %i", g.ballsRemaining );
  for (bufp = buffer; *bufp; bufp++)
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

  glColor3f(1.0, 1.0, 0.0);
  glRasterPos2i( 40, h - 70 );
  snprintf(buffer, sizeof buffer, "Score: %i", g.score );
  for (bufp = buffer; *bufp; bufp++)
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);


  if ( g.gameOver )
  {
	  glColor3f(1.0, 1.0, 0.0);
	  glRasterPos2i( 0.5 * w - 45, 0.5 * h + 40);
	  snprintf(buffer, sizeof buffer, "Game Over");
	  for (bufp = buffer; *bufp; bufp++)
	    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

	  glColor3f(1.0, 1.0, 0.0);
	  glRasterPos2i( 0.5 * w - 80, 0.5 * h + 20 );
	  snprintf(buffer, sizeof buffer, "You scored %5i", g.score);
	  for (bufp = buffer; *bufp; bufp++)
	    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

	  glColor3f(1.0, 1.0, 0.0);
	  glRasterPos2i( 0.5 * w - 90, 0.5 * h );
	  snprintf(buffer, sizeof buffer, "Press 'r' to RESTART");
	  for (bufp = buffer; *bufp; bufp++)
	    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);
	}

	if ( g.extraBall )
	{
		glColor3f(1.0, 1.0, 0.0);
	  glRasterPos2i( 0.5 * w - 140, 0.5 * h + 40);
	  snprintf(buffer, sizeof buffer, "Nice shot, you scored an extra ball");
	  for (bufp = buffer; *bufp; bufp++)
	    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

	  glColor3f(1.0, 1.0, 0.0);
	  glRasterPos2i( 0.5 * w - 45, 0.5 * h + 20 );
	  snprintf(buffer, sizeof buffer, "+%i Points", CATCHERBONUS);
	  for (bufp = buffer; *bufp; bufp++)
	    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);
	}

  glPopMatrix();  /* Pop modelview */
  glMatrixMode(GL_PROJECTION);

  glPopMatrix();  /* Pop projection */
  glMatrixMode(GL_MODELVIEW);

  glPopAttrib();
}

void display()
{
	GLenum err;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Draw gameBoard and pegs
	glPushMatrix();

		drawBall();
		drawCatcher();

		/*
		 * Draw the direction vector ONLY when the ball is NOT in motion
		 * as the vector is drawn from the ball coordinates, and is ONLY
		 * needed before the ball is launched
	   */
		if ( !gameStart )
		{
			drawVector();
			drawTrajectory();
		}
		// Draw the edges of the gameBoard
		gameBoard->drawEdge();

		// Draw each of the pegs
		gameBoard->drawBoard( );	

	glPopMatrix();

	displayOSD();

	glutSwapBuffers();

	// Always check for errors
	while ((err = glGetError()) != GL_NO_ERROR)
		std::cout << gluErrorString(err) << std::endl;
}

void update()
{
	static int lastTime = -1;
	
	if (lastTime < 0)
		lastTime = glutGet(GLUT_ELAPSED_TIME);

	t = glutGet(GLUT_ELAPSED_TIME);
	int dtMs = t - lastTime;
	dt = (float)dtMs / 1000.0f;
	lastTime = t;

	if ( !g.gameOver )
		moveCatcher( dt );

	if ( gameStart )
	{
		integrate( &position, &velocity );
		collide( &position, &velocity, false );
	}

	if ( debug.collisions )
		std::cout << "t = " << t << "  dt = " << dt << "  velocity.y = " << velocity.y << std::endl;

	glutPostRedisplay();
}

/*
 * Cycling through the different board types, unfortunately I have been unable
 * to iterate through the enum array in NON magic numbers way. IE if the order
 * of the boardType is ever modified, then this function will need to be
 * modifed as well, I would have prefered to use something like 
 * gameBoard->currentBoardType + 1 and cycle through the array that way
 *
 *     //TODO############  ADD A delete previous gameboard, before new one is created
 */
void changeBoard()
{
	cleanup();

	switch( gameBoard->currentBoardType )
  {
  	case board::boardType::GRID:
  		gameBoard = new board( board::boardType::OFFGRID, numPegs );
  	break;

  	case board::boardType::OFFGRID:
  		gameBoard = new board( board::boardType::TESTGRID, numPegs );
  	break;

   	case board::boardType::TESTGRID:
  		gameBoard = new board( board::boardType::GRID, numPegs );
  	break; 	
  }
}

/*
 * Used to redraw the board, after the player has increased or decreased the
 * number of pegs
 */
void redrawBoard()
{	
	cleanup();

	switch( gameBoard->currentBoardType )
  {
  	case board::boardType::GRID:
  		gameBoard = new board( board::boardType::GRID, numPegs );
  	break;

  	case board::boardType::OFFGRID:
  		gameBoard = new board( board::boardType::OFFGRID, numPegs );
  	break;

   	case board::boardType::TESTGRID:
  		gameBoard = new board( board::boardType::TESTGRID, numPegs );
  	break; 	
  }
}

void myInit () 
{
	gameBoard = new board( board::boardType::GRID, numPegs );

	// Set radius of ball
	radius = 0.2;

	// Generate the catcher shape
	calcCatcherShape();

	// Initialize the ball variables
	resetBall();

	// Change between filled and wireframe
	setRenderMode(renMode);

	calculateTrajectory();
}

int main ( int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(900, 900);
	glutInitWindowPosition(250, 100);
	glutCreateWindow("Assignment 3 - Peggle");
	glutKeyboardFunc(keyboardCB);
	glutReshapeFunc(myReshape);
	glutDisplayFunc(display);
	glutIdleFunc(update);

	myInit();

	glutMainLoop();
}

void cleanup( )
{
	delete gameBoard;
}