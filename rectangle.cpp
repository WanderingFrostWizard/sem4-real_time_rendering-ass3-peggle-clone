
#include "rectangle.h"

#include <iostream>

rectangle::rectangle( glm::vec3 _pegCentre )// : public peg::peg()
{
	pegCentre = _pegCentre;

	calculateVerts( pegCentre, 
			&topLeft, 
			&topRight, 
			&bottomLeft, 
			&bottomRight,
			height,
			width );

	collision = false;
}

bool rectangle::checkImpact( glm::vec3 position, float radius, bool isFuture )
{

	#if 0
	glm::vec3 position;
	float radius;

	
	float ballLHS = position.x - radius;
	float ballRHS = position.x + radius;
	float ballTop = position.y + radius;
	float ballBottom = position.y - radius;

	// Check for impact to top edge
	if ( ballBottom < topLeft.y )
		std::cout << " Top EDGE " << std::endl;
	// Check for impact to bottom edge
	else if ( ballTop > bottomLeft.y )
		std::cout << " Bottom EDGE " << std::endl;

	// Check for impact to Right edge
	else if ( ballLHS < bottomRight.x )
		std::cout << " Right EDGE " << std::endl;
	// Check for impact to Left edge
	else if ( ballRHS > bottomLeft.x )
		std::cout << " Left EDGE " << std::endl;

	// Check for if ball pos+radius is within the boundaries of the rectangle ie < top, > bottom, < rhs, > lhs etc
	// std::cout << " test ";

	#endif 
	return false;
}

void rectangle::draw( )
{
	glPushAttrib(GL_CURRENT_BIT);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, rectColor );
	glBegin(GL_QUAD_STRIP);
		glColor3f ( 0.0, 0.75, 0.0 );
		glVertex3fv(&topLeft[0]);
		glVertex3fv(&topRight[0]);
		glVertex3fv(&bottomLeft[0]);
		glVertex3fv(&bottomRight[0]);
	glEnd();
	glPopAttrib();
}