
#pragma once

#include <glm/glm.hpp>	// for vec3

// Function to calculate the vertices from a center point, using width and height
static void calculateVerts( glm::vec3 pegCentre, 
			glm::vec3 * topLeft, 
			glm::vec3 * topRight, 
			glm::vec3 * bottomLeft, 
			glm::vec3 * bottomRight,
			float height,
			float width )
{
	// Top Edges
	topLeft->x = pegCentre.x - (0.5 * width);
	topLeft->y = pegCentre.y + (0.5 * height);
	topLeft->z = 0;

	topRight->x = pegCentre.x + (0.5 * width);
	topRight->y = pegCentre.y + (0.5 * height);
	topRight->z = 0;

	// Bottom Edges
	bottomLeft->x = pegCentre.x - (0.5 * width);
	bottomLeft->y = pegCentre.y - (0.5 * height);
	bottomLeft->z = 0;

	bottomRight->x = pegCentre.x + (0.5 * width);
	bottomRight->y = pegCentre.y - (0.5 * height);
	bottomRight->z = 0;
}
